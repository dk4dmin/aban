#define PERL_NO_GET_CONTEXT

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include <openssl/evp.h>
#include <openssl/err.h>

#define NONCE_SIZE (64/8)

int spew_openssl_err(const char *str, size_t len, void *when) {
	warn("openssl: %s during %s", str, (char *)when);
	return 0;
}

int foo_prf(const EVP_CIPHER *cipher,
            uint64_t block,
            unsigned char *key,   size_t key_buflen,
            char *nonce, size_t nonce_buflen,
            unsigned char *zero,  size_t zero_buflen,
            size_t off,  size_t size, unsigned char *out, size_t out_buflen) {

    EVP_CIPHER_CTX *ctx;
    unsigned char iv[128/8];
    unsigned int ctx_keylen;
    int outlen;

	if (nonce_buflen != NONCE_SIZE) {
		croak("Nonce is %zu bytes, not expected %zu",
		      nonce_buflen, (size_t)NONCE_SIZE);
		return 0;
	}

	if (zero_buflen < size) {
		croak("Zero block is too small (is %zu, need %zu)",
		      zero_buflen, size);
		return 0;
	}

	if (off + size > out_buflen) {
		croak("Asked to overflow, offset=%zu, length=%zu, buffer_size=%zu",
		      off, size, out_buflen);
		return 0;
	}

	if (!(ctx = EVP_CIPHER_CTX_new())) {
		ERR_print_errors_cb(spew_openssl_err, "CTX_new");
		croak("EVP_CIPHER_CTX_new failed");
		return 0;
	}

	/* hopefully OpenSSL still works with the full 64-bit counter, not
	 * just the reduced 32-bit one... */
	memcpy(iv+0, &block, 8);
	memcpy(iv+8, nonce, 8);

	if (!EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv)) {
		ERR_print_errors_cb(spew_openssl_err, "EncryptInit_ex");
		EVP_CIPHER_CTX_free(ctx);
		croak("EVP_EncryptInit_ex failed");
	}
	ctx_keylen = EVP_CIPHER_CTX_key_length(ctx);

	if (key_buflen != ctx_keylen) {
		EVP_CIPHER_CTX_free(ctx);
		croak("Key is %zu bytes, not expected %u", key_buflen, ctx_keylen);
		return 0;
	}

	if (16 != EVP_CIPHER_CTX_iv_length(ctx)) {
		EVP_CIPHER_CTX_free(ctx);
		croak("BUG: ctx iv length is not 16");
		return 0;
	}

	EVP_CIPHER_CTX_set_padding(ctx, 0);
	if (!EVP_EncryptUpdate(ctx, out+off, &outlen, zero, size)) {
		ERR_print_errors_cb(spew_openssl_err, "EncryptUpdate");
		EVP_CIPHER_CTX_free(ctx);
		croak("EVP_EncryptUpdate failed");
		return 0;
	}

	if (outlen != size) {
		croak("Did not do all work; wanted = %zu, got %i", size, outlen);
		return 0;
	}

	EVP_CIPHER_CTX_free(ctx);

	return 1;
}


MODULE = App::ABAN::PRF::OpenSSL			PACKAGE = App::ABAN::PRF::OpenSSL

PROTOTYPES: DISABLED

int
chacha_prf(UV block,                                      \
           char *key, size_t length(key),                 \
           char *nonce, size_t length(nonce),             \
           char *zeroblock, size_t length(zeroblock),     \
           size_t off, size_t size, SV *out_sv)
PREINIT:
	char *out;
	size_t out_len;
CODE:
	if (sizeof(block) < 8)
		croak("UV isn't 64-bit or larger; FIXME");
	out = SvPV_force(out_sv, out_len);
	RETVAL = foo_prf(EVP_chacha20(), block,
	                 (unsigned char *)key, XSauto_length_of_key,
	                 nonce, XSauto_length_of_nonce,
	                 (unsigned char *)zeroblock, XSauto_length_of_zeroblock,
	                 off, size, (unsigned char *)out, out_len);
OUTPUT:
	RETVAL

int
aes256_prf(UV block,                                      \
           char *key, size_t length(key),                 \
           char *nonce, size_t length(nonce),             \
           char *zeroblock, size_t length(zeroblock),     \
           size_t off, size_t size, SV *out_sv)
PREINIT:
	char *out;
	size_t out_len;
CODE:
	if (sizeof(block) < 8)
		croak("UV isn't 64-bit or larger; FIXME");
	out = SvPV_force(out_sv, out_len);
	RETVAL = foo_prf(EVP_aes_256_ctr(), block,
	                 (unsigned char *)key, XSauto_length_of_key,
	                 nonce, XSauto_length_of_nonce,
	                 (unsigned char *)zeroblock, XSauto_length_of_zeroblock,
	                 off, size, (unsigned char *)out, out_len);
OUTPUT:
	RETVAL
