package App::ABAN::DumbUI;
#ABSTRACT: ABAN text-mode UI for a "dumb" terminal
use 5.028;
use warnings qw(all);
use strict;

use POE qw(Wheel::ReadWrite Filter::Stream);
use Term::ReadKey qw(ReadMode);
use Data::Dump qw(pp);
use Text::ASCIITable;


sub create {
	POE::Session->create(
		package_states  => [
			(__PACKAGE__) => {
				_start => '_start',
				_stop => '_stop',
				keystroke => '_keystroke',
				
				# our API
				set_status => '_set_status',
				set_wipeinfo => '_set_wipeinfo',
				set_protected => '_set_protected',
				set_drives => '_set_drives',
				set_victims => '_set_victims',
				set_benchmark => '_set_benchmark',
				set_prf => '_set_prf',

				wipe_progress => '_wipe_progress',
				wipe_complete => '_wipe_complete',

				shutdown => '_shutdown',

			},
		]
	);
}

sub _start {
	$_[KERNEL]->alias_set('UI');
	$_[HEAP]{StdIO} = POE::Wheel::ReadWrite->new(
		InputHandle => \*STDIN,
		OutputHandle => \*STDOUT,
		InputFilter => POE::Filter::Stream->new(),
		OutputFilter => POE::Filter::Line->new(Literal => "\n"),
		InputEvent => 'keystroke',
	);
	ReadMode 'cbreak';

	$_[KERNEL]->post(MAIN => 'ui_ready');
}

sub _stop {
	ReadMode 'restore';
}

sub _keystroke {
	my $key = $_[ARG0];
	my $io = $_[HEAP]{StdIO};

	if ('h' eq $key || '?' eq $key) {
		$io->put(<<KEY_HELP);
Keys:
   h, ? - this message
   s - re-display last status message
   v - display list of devices we're wiping (victims)
   p - display list of protected devices (what we're booted from)
   d - display list of possible devices (wipe unless protected)
   b - display PRF benchmark results

Control-c: Abort. Note that data already wiped is unrecoverable. If
           aborted after the wipe has started, just results in a partial job.
KEY_HELP
	} elsif ('s' eq $key) {
		$io->put("Current status: $_[HEAP]{app_status}");
	} elsif ('p' eq $key) {
		$io->put("Protected devices: " . pp($_[HEAP]{app_protected}));
	} elsif ('d' eq $key) {
		$io->put("All potential disks: " . pp($_[HEAP]{app_drives}));
	} elsif ('v' eq $key) {
		$io->put("Victims: " . pp($_[HEAP]{app_victims}));
	} elsif ('b' eq $key) {
		$io->put("PRF Benchmark: " . pp($_[HEAP]{app_benchmark}));
		$io->put("PRF Chosen: $_[HEAP]{app_prf}.");
	} else {
		$io->put("Unknown key: $key");
	}

	return;
}

sub _shutdown {
	$_[HEAP]{StdIO}->flush; # give it a try...
	$_[HEAP]{StdIO} = undef; # session idle after this, will exit.
}

sub _set_status {
	$_[HEAP]{app_status} = $_[ARG0];
	$_[HEAP]{StdIO}->put("Status: $_[ARG0]");
	return;
}

sub _set_wipeinfo {
	$_[HEAP]{app_wipeinfo} = $_[ARG0];
	return;
}

sub _set_protected {
	$_[HEAP]{app_protected} = $_[ARG0];
	$_[HEAP]{StdIO}->put("Recevied protected device list; p to display");
	return;
}

sub _set_drives {
	$_[HEAP]{app_drives} = $_[ARG0];
	$_[HEAP]{StdIO}->put("Recevied drive list; d to display");
	return;
}

sub _set_victims {
	$_[HEAP]{app_victims} = $_[ARG0];
	$_[HEAP]{StdIO}->put("Recevied list of drives to wipe (victims); v to re-display:", pp $_[ARG0]);
	return;
}

sub _set_benchmark {
	$_[HEAP]{app_benchmark} = $_[ARG0];
	$_[HEAP]{StdIO}->put("Recevied PRF benchmark; b to display");
	return;
}

sub _set_prf {
	$_[HEAP]{app_prf} = $_[ARG0];
	$_[HEAP]{StdIO}->put("Chosen PRF is $_[ARG0].");
	return;
}

sub _wipe_progress {
	my ($ident, $msg) = @_[ARG0..ARG1];

	$_[HEAP]{StdIO}->put("<$ident> $msg");
}

sub _wipe_complete {
	$_[HEAP]{StdIO}->put("ABAN is done wiping disks.");

	my $t = Text::ASCIITable->new(
		{headingText => "Final ABAN Disk Wiper Results"});
	$t->setCols("Disk", "Speed(W)", "Speed(V)", "Unwiped", "Unverified",
		"Notes");
	$t->alignCol("Unwiped", 'right');
	$t->alignCol("Unverified", 'right');
	
	foreach my $info (sort { $a->{victim}{node} cmp $b->{victim}{node} }
		@{$_[HEAP]{app_wipeinfo}})
	{
		$t->addRow(
			$info->{victim}{node},
			$info->{wipe_complete_info}{rate},
			$info->{verify_complete_info}{rate},
			$info->{wipe_complete_info}{worklist},
			$info->{verify_complete_info}{worklist},
			($info->{errored_info} ? "DIED ($info->{errored_info})" : ""),
		);
	}

	$_[HEAP]{StdIO}->put("$t");

	return;
}

1;
