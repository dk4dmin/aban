package App::ABAN::WipeChild;
#ABSTRACT: ABAN internal POE session to track a single disk
use 5.028;
use warnings qw(all);
use strict;

use POE qw(Wheel::Run);

sub create {
	my (undef, $type, $ident, $algo, $disk) = @_;

	my @cmd;
	my %handlers = (
		_start       => 'start_session',
		child_stderr => 'child_stderr',
		child_close  => 'child_close',
		child_signal => 'child_signal',
		child_error  => 'child_error',
		abort        => 'kill_child',
	);
	if ('speed' eq $type) {
		defined $algo and die "BUG: for speed test, no algorithm expected";
		defined $disk and die "BUG: speed test does not take a disk";
		$handlers{child_stdout} = 'child_stdout_speed';
		$handlers{send_done} = 'send_done_speed';
		$handlers{send_failed} = 'send_failed_speed';
		@cmd = qw(wipe-disk --speed);
	} elsif ('wipe' eq $type) {
		defined $algo or die "BUG: wipe mode needs an algorithm";
		defined $disk or die "BUG: wipe mode needs a disk";
		$handlers{send_done} = 'send_done_wipe';
		$handlers{send_failed} = 'send_failed_wipe';
		$handlers{child_stdout} = 'child_stdout_wipe';
		@cmd = (qw(wipe-disk --wipe --verify --prf), $algo, '--disk', $disk );
	} else {
		die "BIG: unknown WipeChild type '$type'";
	}

	return POE::Session->create(
		args => [ $ident, \@cmd ],
		package_states => [ __PACKAGE__, \%handlers ]
	);
}

sub start_session {
	$_[HEAP]{ident} = $_[ARG0];
	$_[KERNEL]->alias_set("WipeChild-$_[HEAP]{ident}");

	my $child = POE::Wheel::Run->new(
		Program => $_[ARG1],
		StdoutEvent => 'child_stdout',
		StderrEvent => 'child_stderr',
		CloseEvent => 'child_close',
		ErrorEvent => 'child_error',
		NoStdin => 1,
	);
	$_[KERNEL]->sig_child($child->PID, 'child_signal');
	$_[HEAP]{child} = $child;

	return;
}

sub send_done_speed {
	$_[KERNEL]->post(MAIN => speed_results => $_[HEAP]{speed});
	return;
}

sub send_failed_speed {
	$_[KERNEL]->post(MAIN => speed_failed => $_[ARG0]);
	return;
}

sub send_done_wipe {
	$_[KERNEL]->post(MAIN => wipe_child_done => $_[HEAP]{ident});
	return;
}

sub send_failed_wipe {
	$_[KERNEL]->post(MAIN => wipe_child_fail => $_[HEAP]{ident}, $_[ARG0]);
	return;
}

sub child_stderr {
	$_[KERNEL]->post(UI => wipe_progress => $_[HEAP]{ident}, $_[ARG0]);
	return;
}

sub child_close { }

sub child_signal { 
	# remove both refs to allow GC
	$_[HEAP]{child} = undef; 
	$_[KERNEL]->alias_remove("WipeChild-$_[HEAP]{ident}");

	# and send that we're done.
	my $status = $_[ARG2];
	if (0 == $status) {
		$_[KERNEL]->yield('send_done');
	} else {
		$_[KERNEL]->yield('send_failed', $status);
	}
}
sub child_error {
	my ($op, $errnum, $errstr, $wheel_id) = @_[ARG0..ARG3];
	return if ($op eq 'read' && $errnum == 0); # EOF is OK
	warn "Child error! op $op, error $errstr";
}

sub _parse_kv_pairs {
	my $pairs = shift;
	my %h;

	while ($pairs =~ s/^(\w+)=\{([^}]*)\}\s*/($h{$1}=$2),''/e) { };

	return \%h;
}

sub child_stdout_speed { 
	my $kvdat;
	if ($_[ARG0] =~ /^SPEED:\s+(.+)$/) {
		$kvdat = $1
	} else {
		warn "Invalid --speed line: $_[ARG0]";
		return;
	}

	my $data = _parse_kv_pairs($kvdat);
	$_[HEAP]{speed}{$data->{alg}} = $data;

	return;
}

sub child_stdout_wipe {
	my ($msg, $kv);

	if ($_[ARG0] =~ /^([A-Z_]+)[:;]\s+(.+)$/) {
		$msg = $1;
		$kv = $2;
	} else {
		warn "Invalid wipe-disk line: $_[ARG0]";
		return;
	}

	if ($msg =~ /^(WIPE|VERIFY)_(START|COMPLETE)$/n) {
		$_[KERNEL]
			->post(MAIN => lc($msg), $_[HEAP]{ident}, _parse_kv_pairs($kv));
	} else {
		warn "Unknown message $msg (in $_[ARG0])";
	}
	return;
}

sub kill_child {
	my $child = $_[HEAP]{child};
	$child->kill if $child;
}


1;

=head1 DESCRIPTION

A separate L<POE::Session> is created for each disk being wiped to
manage the wipe-disk child-process (via L<POE::Wheel::Run>). This module
is that session.

The main thing it does is keep track of the child and pass back status
to the main session. Nothing here should have much use outside of ABAN.

=head1 SEE ALSO

=over 4

=item L<ABAN>

For detail about what ABAN is, see the main L<ABAN> manual page/perldoc.

=back
